package app.repository;

import app.model.Sensor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SensorRepository extends CrudRepository<Sensor, Long> {
    Sensor findById(int id);
    Sensor findBySerial(String serial);
}
