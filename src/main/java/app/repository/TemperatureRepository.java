package app.repository;

import app.model.Temperature;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TemperatureRepository extends CrudRepository<Temperature, Long> {
    Temperature findById(int id);
    List<Temperature> findBySensorId(int sensorId);
}
