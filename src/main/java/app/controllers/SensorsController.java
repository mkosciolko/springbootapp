package app.controllers;

import app.exceptions.InvalidRequestException;
import app.exceptions.SensorAlreadyExistsException;
import app.exceptions.SensorNotFoundException;
import app.model.Sensor;
import app.repository.SensorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/api")
public class SensorsController {

    // TODO: protect POST endpoints from accepting JSONs with ID field

    @Autowired
    private SensorRepository sensorRepository;

    @RequestMapping(value = "/sensor", method = RequestMethod.POST)
    Sensor addSensor(@RequestBody Sensor sensor, HttpServletResponse httpResponse, WebRequest request) throws SensorAlreadyExistsException {
        if (sensorRepository.findBySerial(sensor.getSerial()) != null) {
            throw new SensorAlreadyExistsException();
        } else {
            Sensor createdSensor = null;
            createdSensor = sensorRepository.save(sensor);
            httpResponse.setStatus(HttpStatus.CREATED.value());
            httpResponse.setHeader("Location", String.format("%s/show/sensor/id/%s", request.getContextPath(), sensor.getId()));
            return createdSensor;
        }
    }

    @RequestMapping(value = "/sensor/{id}", method = RequestMethod.PUT)
    Sensor updateSensor(@RequestBody Sensor sensor, @PathVariable int id, HttpServletResponse httpResponse) throws SensorNotFoundException, InvalidRequestException {
        Sensor sensorToUpdate = null;
        if (sensorRepository.findById(id) != null) {
            if (sensor.getId() == id) {
                sensorToUpdate = sensorRepository.save(sensor);
                httpResponse.setStatus(HttpStatus.OK.value());
                return sensorToUpdate;
            } else {
                throw new InvalidRequestException();
            }
        } else {
            throw new SensorNotFoundException();
        }
    }

    @RequestMapping(value = "/sensor/{id}", method = RequestMethod.DELETE)
    void removeSensor(@PathVariable int id, HttpServletResponse httpResponse) throws SensorNotFoundException {
        Sensor sensorToRemove = sensorRepository.findById(id);
        if (sensorToRemove != null) {
            sensorRepository.delete(sensorToRemove);
            httpResponse.setStatus(HttpStatus.NO_CONTENT.value());
        } else {
            throw new SensorNotFoundException();
        }
    }

    @RequestMapping(value = "/sensor", method = RequestMethod.GET)
    @ResponseBody
    Collection<Sensor> showSensors() {
        return (Collection<Sensor>) sensorRepository.findAll();
    }

    @RequestMapping(value = "/sensor/{id}", method = RequestMethod.GET)
    @ResponseBody
    Sensor showSensorById(@PathVariable int id) throws SensorNotFoundException {
        Sensor sensor = sensorRepository.findById(id);
        validateSensor(sensor);
        return sensor;
    }

    @RequestMapping(value = "/sensor/serial/{serial}", method = RequestMethod.GET)
    @ResponseBody
    Sensor showSensorBySerial(@PathVariable String serial) throws SensorNotFoundException {
        Sensor sensor = sensorRepository.findBySerial(serial);
        validateSensor(sensor);
        return sensor;
    }

    private void validateSensor(Sensor sensor) throws SensorNotFoundException {
        if (sensor == null) {
            throw new SensorNotFoundException();
        }
    }
}
