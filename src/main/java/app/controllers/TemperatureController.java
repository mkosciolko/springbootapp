package app.controllers;

import app.exceptions.InvalidRequestException;
import app.exceptions.TemperatureNotFoundException;
import app.model.Temperature;
import app.repository.TemperatureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/api")
public class TemperatureController {

    // TODO: protect POST endpoints from accepting JSONs with ID field

    @Autowired
    private TemperatureRepository temperatureRepository;

    @RequestMapping(value = "/temperature", method = RequestMethod.POST)
    Temperature addTemperature(@RequestBody Temperature temperature, HttpServletResponse httpResponse, WebRequest request) {
        Temperature createdTemperature = null;
        createdTemperature = temperatureRepository.save(temperature);
        httpResponse.setStatus(HttpStatus.CREATED.value());
        httpResponse.setHeader("Location", String.format("%s/show/temperature/id/%s", request.getContextPath(), temperature.getSensorId()));
        return createdTemperature;
    }

    @RequestMapping(value = "/temperature/{id}", method = RequestMethod.PUT)
    Temperature updateTemperature(@RequestBody Temperature temperature, @PathVariable int id, HttpServletResponse httpResponse) throws InvalidRequestException, TemperatureNotFoundException {
        Temperature temperatureToUpdate = null;
        if (temperatureRepository.findById(id) != null) {
            if (temperature.getId() == id) {
                temperatureToUpdate = temperatureRepository.save(temperature);
                httpResponse.setStatus(HttpStatus.OK.value());
                return temperatureToUpdate;
            } else {
                throw new InvalidRequestException();
            }
        } else {
            throw new TemperatureNotFoundException();
        }
    }

    @RequestMapping(value = "/temperature/{id}", method = RequestMethod.DELETE)
    void removeTemperature(@PathVariable int id, HttpServletResponse httpResponse) throws TemperatureNotFoundException {
        Temperature temperatureToRemove = temperatureRepository.findById(id);
        if (temperatureToRemove != null) {
            temperatureRepository.delete(temperatureToRemove);
            httpResponse.setStatus(HttpStatus.NO_CONTENT.value());
        } else {
            throw new TemperatureNotFoundException();
        }
    }

    @RequestMapping(value = "/temperature", method = RequestMethod.GET)
    @ResponseBody
    Collection<Temperature> showTemp() {
        return (Collection<Temperature>) temperatureRepository.findAll();
    }

    @RequestMapping(value = "/temperature/{id}", method = RequestMethod.GET)
    @ResponseBody
    Temperature showTempById(@PathVariable int id) throws TemperatureNotFoundException {
        Temperature temperature = temperatureRepository.findById(id);
        validateTemperature(temperature);
        return temperature;
    }

    @RequestMapping(value = "/temperature/sensor/{sensorId}", method = RequestMethod.GET)
    @ResponseBody
    Collection<Temperature> showTempBySensorId(@PathVariable int sensorId) throws TemperatureNotFoundException {
        List<Temperature> temperature = temperatureRepository.findBySensorId(sensorId);
        validateTemperature(temperature);
        return temperature;
    }

    private void validateTemperature(Temperature temperature) throws TemperatureNotFoundException {
        if (temperature == null) {
            throw new TemperatureNotFoundException();
        }
    }

    private void validateTemperature(List<Temperature> temperature) throws TemperatureNotFoundException {
        if (temperature.isEmpty()) {
            throw new TemperatureNotFoundException();
        }
    }
}
