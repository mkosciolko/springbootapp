package app.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "sensors")
public class Sensor extends AbstractEntity {

    @Column
    private String serial;

    protected Sensor() {}

    public Sensor(final String serial) {
        this.serial = serial;
    }

    @Override
    public String toString() {
        return "SensorEntity{" +
                "ID=" + getId() +
                ", SERIAL='" + serial + '\'' +
                '}';
    }
}
