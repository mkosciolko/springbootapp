package app.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "temperature")
public class Temperature extends AbstractEntity {

    @Column(name = "sensor_id")
    private int sensorId;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date time;

    @Column
    private float temperature;

    protected Temperature() {}

    public Temperature(final int sensorId, final Date time, final float temperature) {
        this.sensorId = sensorId;
        this.time = time;
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "TemperatureEntity{" +
                "ID=" + getId() +
                ", SENSOR_ID='" + sensorId + '\'' +
                ", TIME='" + time + '\'' +
                ", TEMPERATURE=" + temperature +
                '}';
    }
}
